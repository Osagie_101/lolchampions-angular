import { Component, OnInit } from '@angular/core';
import { LolChampion } from '../shared/lol-champion';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-champion-list',
  templateUrl: './champion-list.component.html',
  styleUrls: ['./champion-list.component.css']
})
export class ChampionListComponent implements OnInit {

  champions: LolChampion[] = [];

  constructor(private restApi: RestApiService) { }

  ngOnInit(): void {
    this.loadChampions();
  }

  loadChampions() {
    return this.restApi.getChampions().subscribe((data: LolChampion[]) => {
        this.champions = data;
    })
  }
}
