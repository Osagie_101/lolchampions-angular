import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChampionListComponent } from './champion-list/champion-list.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'champion-list' },
  { path: 'champion-list', component: ChampionListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
